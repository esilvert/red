doc:
	find 'lib/red' -name '*.rb' | xargs rdoc --markup=rdoc -o public/doc

clear_doc:
	rm -r public/doc/

regenerate_doc: clear_doc doc
