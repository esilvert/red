# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Red::Buffers::History, type: :unit do
  before(:each) { clear_managers }

  subject { Red::Buffers::History.new }

  describe '.content' do
    let(:test_line) { 'This is a test line' }

    it 'starts empty' do
      expect(subject.content).to be_empty
    end

    it 'stacks history events' do
      subject
      Red::EventManager.instance.trigger(:history, test_line)
      expect(subject.content.chomp).to eq(test_line)
    end
  end
end
