# frozen_string_literal: true

require 'bundler/setup'

begin
  require 'red'
rescue StandardError => e
  pp e.class, e.message
end

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  ENV['RED_ENV'] ||= Red::Env::TEST

  def listener_to_event(event_name)
    OpenStruct.new.tap do |item|
      Red::EventManager.instance.listen_to(item, event_name, proc do |listener|
                                                               listener.triggered = true
                                                             end)
    end
  end

  def clear_managers
    Red::BufferManager.instance.clear_stack
    Red::BufferManager.instance.clear_stack

    Red::EventManager.instance.clear_listeners
    Red::EventManager.instance.clear_listeners
  end
end
