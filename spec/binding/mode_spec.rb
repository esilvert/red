# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Red::Binding::Mode, type: :unit do
  subject { Red::Binding::Mode.new }

  it { is_expected.to respond_to(:on_enabled) }
end
