# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Red::EventManager, type: :unit do
  subject { Red::EventManager.instance.instance_variable_get(:@subscriptions) }

  before(:each) { clear_managers }
  after(:each) { clear_managers }

  describe '.new' do
    it { is_expected.to be_empty }
  end

  describe '.listen_to' do
    it 'remembers new listeners' do
      expect(subject).to be_empty

      listener = listener_to_event('test')

      expect(subject).not_to be_empty
      expect(subject['test'].first[:listener]).to eq(listener)
      expect(subject['test'].first[:callback]).to be_a(Proc)
    end
  end

  describe '.trigger' do
    it 'works' do
      expect(subject).to be_empty

      listener = listener_to_event('test')

      Red::EventManager.instance.trigger('test')

      expect(listener.triggered).to be_truthy
    end
  end
end
