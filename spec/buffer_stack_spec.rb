# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Red::BufferStack, type: :unit do
  subject { Red::BufferStack.new }

  describe '.current_buffer' do
    it 'is starts empty' do
      expect(subject.empty?).to be_truthy
    end
  end

  describe '.find_by_name' do
    before(:each) { subject << Red::Buffer.new.with_path('test/test.txt').infos }

    it 'works' do
      expect(subject.find_by_name('test.txt')).not_to be_nil
    end

    it 'returns nil on failure' do
      expect(subject.find_by_name('not_found')).to be_nil
    end
  end

  describe '.find_by_special_name' do
    before(:each) { subject << Red::Buffers::History.new.infos }

    it 'works' do
      expect(subject.find_by_special_name('*history*')).not_to be_nil
    end

    it 'returns nil on failure' do
      expect(subject.find_by_special_name('not_found')).to be_nil
    end
  end
end
