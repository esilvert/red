# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Red::Buffer, type: :unit do
  subject { Red::Buffer }

  describe '.new' do
    context 'with path' do
      subject { Red::Buffer.new.with_path('test/text.txt') }

      it 'has no content' do
        expect(subject.content).to be_empty
      end
    end

    context 'without path' do
      subject { Red::Buffer.new }

      it 'has no content' do
        expect(subject.content).to be_empty
      end
    end
  end

  describe '.infos' do
    let(:path) { 'test/text.txt' }
    let(:buffer) { Red::Buffer.new.with_path(path) }
    let(:infos) { buffer.infos }

    it 'return a valid BufferInfo' do
      expect(infos.buffer).to eq(buffer)
      expect(infos.key).to eq(buffer.key)
      expect(infos.path).to include(path)
      expect(infos.readonly).to be_falsy
    end
  end

  describe '.save!' do
    let(:test_file) { 'test/test/txt' }

    it 'should create a file in the system' do
      buffer = subject.new.with_path(test_file)

      expect { buffer.save! }.not_to raise_error
      expect(File.file?(test_file)).to be_truthy
    end
  end

  describe '.readonly' do
    subject { Red::Buffer.new }
    it { is_expected.not_to be_readonly }
  end

  describe '.on_modified' do
    it 'should update content' do
      text = double
      allow(text).to receive(:get).and_return('This is text content')

      buffer = subject.new.with_path('test/text.txt')
      expect { buffer.on_modified(text) }.not_to raise_error
      expect(buffer.content).to eq(text.get)
    end
  end

  describe '.key' do
    subject { Red::Buffer.new.with_path('test/test.txt') }

    it 'removes module' do
      expect(subject.key.include?('::')).to be_falsy
    end
  end
end
