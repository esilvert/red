# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Red::BufferManager, type: :unit do
  subject { Red::BufferManager.instance }

  before(:each) { clear_managers }
  after(:each) { clear_managers }

  describe '.buffer_stack' do
    it { is_expected.not_to respond_to(:buffer_stack=) }

    it { is_expected.to respond_to(:buffer_stack) }

    # ---------------------------------------------
    # Just initialized
    # ---------------------------------------------
    context 'when just initialized' do
      subject { Red::BufferManager.instance.buffer_stack }

      it { is_expected.to be_a(Red::BufferStack) }
      it { expect(subject.size).to eq(0) }
    end

    # ---------------------------------------------
    # FIND OR CREATE
    # ---------------------------------------------
    describe '.find_or_create' do
      let(:test_file) { '___Test___.txt' }

      context 'when called without param' do
        subject { Red::BufferManager.instance.find_or_create(nil) }

        it 'should return scratch buffer' do
          expect(subject.path).to include('*scratch*')
        end
      end

      context 'when creating a new buffer' do
        subject { Red::BufferManager.instance.find_or_create(test_file) }

        it { is_expected.to be_a(Red::BufferInfo) }

        it 'should refer to the buffer' do
          expect(subject.basename).to eq(test_file)
        end

        it 'should have registered it in its stack' do
          subject # actually calls it
          expect(Red::BufferManager.instance.buffer_stack.length).to eq(1)
        end

        it 'should NOT have created a file in the system' do
          expect(File.file?(test_file)).to be_falsy
        end

        it 'should NOT create a new buffer if it already exists' do
          _new = Red::BufferManager.instance.find_or_create(subject.key)
          expect(Red::BufferManager.instance.buffer_stack.size).to eq(1)
        end
      end

      # ---------------------------------------------
      # FIND_OR_CREATE_SPECIAL
      # ---------------------------------------------
      describe '.find_or_create_special' do
        subject { Red::BufferManager.instance.find_or_create_special('history') }

        it { is_expected.to be_a(Red::BufferInfo) }

        it 'should refer to the correct buffer' do
          expect(subject.buffer.class).to eq(Red::Buffers::History)
        end

        it 'should have registered it in its stack' do
          subject # actually calls it
          expect(Red::BufferManager.instance.buffer_stack.length).to eq(1)
        end

        it 'should NOT create a new buffer if it already exists' do
          subject
          _new = Red::BufferManager.instance.find_or_create_special('history')

          expect(Red::BufferManager.instance.buffer_stack.size).to eq(1)
        end
      end

      # ---------------------------------------------
      # GET_FULLPATH
      # ---------------------------------------------
      describe '.get_fullpath' do
        subject { Red::BufferManager.instance.get_fullpath('*scratch*') }
        it { is_expected.to eq('*scratch*') }
      end

      # ---------------------------------------------
      # SHOW_BUFFER
      # ---------------------------------------------
      describe '.show_(special_)buffer' do
        let!(:listener) { listener_to_event(Red::BufferManager::Events::CHANGED) }

        context 'when using normal buffer' do
          it 'triggers and event' do
            Red::BufferManager.instance.show_buffer('history')

            expect(listener.triggered).to be_truthy
          end
        end

        context 'when using special buffer' do
          it 'triggers and event' do
            Red::BufferManager.instance.show_buffer('history')

            expect(listener.triggered).to be_truthy
          end
        end
      end

      # ---------------------------------------------
      # SHOW_PREVIOUS/NEXT_BUFFER
      # ---------------------------------------------
      describe '.show_previous/next_buffer' do
        let!(:listener) { listener_to_event(Red::BufferManager::Events::CHANGED) }

        before do
          @buffer_1 = Red::BufferManager.instance.create_buffer('buffer_1')
          @buffer_2 = Red::BufferManager.instance.create_buffer('buffer_2')
          @buffer_3 = Red::BufferManager.instance.create_buffer('buffer_3')
        end

        subject { Red::BufferManager.instance.buffer_stack }

        context 'when using previous buffer' do
          it 'triggers and event' do
            Red::BufferManager.instance.show_previous_buffer

            expect(listener.triggered).to be_truthy
          end

          it 'rotate the stack in the right order' do
            expect(subject.current_buffer.key).to eq(@buffer_1.key)
            Red::BufferManager.instance.show_previous_buffer
            expect(subject.current_buffer.key).to eq(@buffer_3.key)
          end
        end

        context 'when using next buffer' do
          it 'triggers and event' do
            Red::BufferManager.instance.show_next_buffer

            expect(listener.triggered).to be_truthy
          end

          it 'rotate the stack in the right order' do
            expect(subject.current_buffer.key).to eq(@buffer_1.key)
            Red::BufferManager.instance.show_next_buffer
            expect(subject.current_buffer.key).to eq(@buffer_2.key)
          end
        end
      end

      describe '.save...' do
        let(:test_file) { 'test/test.txt' }
        let(:test_file2) { 'test/test_2.txt' }

        context 'when saving one buffer' do
          it 'creates a file on the filesystem' do
            subject.create_buffer(test_file)
            expect { subject.save_current_buffer }.not_to raise_error
            expect(File.file?(test_file)).to be_truthy
          end
        end

        context 'when saving all buffers' do
          it 'creates a file on the filesystem' do
            subject.create_buffer(test_file)
            subject.create_buffer(test_file2)
            expect { subject.save_all_buffers }.not_to raise_error
            expect(File.file?(test_file)).to be_truthy
            expect(File.file?(test_file2)).to be_truthy
          end
        end
      end
    end
  end
end
