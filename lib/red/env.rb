# frozen_string_literal: true

##
# Red namespace
#
module Red
  ##
  # Environment definition
  # Might take value:
  # +Production+:: For release
  # +Development+:: For development
  # +Test+:: During Rspec execution
  #
  module Env
    PRODUCTION = 'production'
    DEVELOPMENT = 'development'
    TEST = 'test'

    def self.development?
      ENV['RED_ENV'] == Env::DEVELOPMENT
    end

    def self.test?
      ENV['RED_ENV'] == Env::TEST
    end

    def self.production?
      ENV['RED_ENV'] == Env::PRODUCTION
    end
  end

  def self.env
    Red::Env
  end
end
