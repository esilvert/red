# frozen_string_literal: true

module Red
  module Buffers
    ##
    # Special Buffer listening to +on_history_event+ and displaying each event on one line
    #
    class History < SpecialBuffer
      def initialize
        super

        EventManager.instance.listen_to(self, :history, :on_history_event)
      end

      def content
        @current.rewind
        @current.read
      end

      def on_history_event(message)
        @current.puts message
      end
    end
  end
end
