# frozen_string_literal: true

module Red
  module Buffers
    ##
    # Special Buffer listing all opened buffers
    #
    class List < SpecialBuffer
      def content
        @content = BufferManager.instance.buffer_stack.map(&:path).join("\n")
      end
    end
  end
end
