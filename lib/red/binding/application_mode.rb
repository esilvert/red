# frozen_string_literal: true

module Red
  module Binding
    ##
    # The default mode triggered for every buffer
    # It defines basic binding
    #
    class ApplicationMode
      def on_enabled(application)
        application.bind('Control-x, Control-c') { quit }
        application.bind('Control-x, Control-b') { Red::Commands::ListBuffers.call }
        application.bind('Control-x, Left')      { Red::Commands::NextBuffer.call }
        application.bind('Control-x, Right')     { Red::Commands::PreviousBuffer.call }
        application.bind('Control-x, Control-s') { Red::Commands::SaveAllBuffers.call }
        application.bind('Control-x, s')         { Red::Commands::SaveCurrentBuffer.call }
        application.bind('Control-x, Control-r') { Red::Commands::RunCommand.call }
      end

      def quit
        BindingManager.instance.application.destroy
      end
    end
  end
end
