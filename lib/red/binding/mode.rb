# frozen_string_literal: true

module Red
  module Binding
    ##
    # Mother class of every mode
    # Override +on_enabled+ to define bindings
    #
    class Mode
      ##
      # Callback - Called when a buffer matches the current mode
      #
      def on_enabled(binder); end
    end
  end
end
