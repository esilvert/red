# frozen_string_literal: true

module Red
  ##
  # Proxy : TkVariable
  # It laso register every instance to the +VariableManager+
  #
  class Variable < TkVariable
    attr_accessor :variable

    def initialize(key, *args)
      super(*args)
      VariableManager.instance.register(key, self)
    end
  end
end
