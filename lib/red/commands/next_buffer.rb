# frozen_string_literal: true

module Red
  module Commands
    ##
    # Command : Show next buffer (in stack)
    #
    class NextBuffer < Red::AbstractCommand
      def define_steps
        step(:show_next_buffer)
      end

      def show_next_buffer
        BufferManager.instance.show_next_buffer
      end
    end
  end
end
