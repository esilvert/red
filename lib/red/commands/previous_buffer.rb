# frozen_string_literal: true

module Red
  module Commands
    ##
    # Command : Show previous buffer (in stack)
    #
    class PreviousBuffer < Red::AbstractCommand
      def define_steps
        step(:show_previous_buffer)
      end

      def show_previous_buffer
        BufferManager.instance.show_previous_buffer
      end
    end
  end
end
