# frozen_string_literal: true

module Red
  module Commands
    ##
    # Command : Show special buffer *List*
    #
    class ListBuffers < Red::AbstractCommand
      def define_steps
        step(:show_buffer_list)
      end

      def show_buffer_list
        BufferManager.instance.show_special_buffer_list
      end
    end
  end
end
