# frozen_string_literal: true

module Red
  module Commands
    ##
    # Command :
    # - Creates an entry
    # - Fetch command typed by user
    # - Run given command
    #
    class RunCommand < Red::AbstractCommand
      def define_steps
        step(:create_entry)
        step(:close_entry)
        step(:run_command)
      end

      def create_entry
        @variable = Variable.new(Help::Constant.unique_id(self))
        variable = @variable # for capture below

        @entry = $app.create_entry do
          textvariable variable
        end.grid(sticky: 'nesw')

        @entry.focus
        @entry.bind('KeyRelease', proc { on_key_pressed })
        @entry.bind('Return', proc { on_command_validated })
      end

      def on_key_pressed
        pp @variable.value
      end

      def on_command_validated
        @command = @variable.value
        next_step
      end

      def close_entry
        @entry.destroy
        next_step
      end

      def run_command
        command = "Red::Commands::#{Help::String.camelize(@variable.value)}"
        Help::String.constantize(command).call
      end
    end
  end
end
