# frozen_string_literal: true

module Red
  module Commands
    ##
    # Command : Asks the +BufferManager+ to save current buffers
    #
    class SaveCurrentBuffer < Red::AbstractCommand
      def define_steps
        step(:save_current_buffer)
      end

      def save_current_buffer
        BufferManager.instance.save_current_buffer
      end
    end
  end
end
