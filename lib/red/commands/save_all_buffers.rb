# frozen_string_literal: true

module Red
  module Commands
    ##
    # Command : Asks the +BufferManager+ to save all buffers
    #
    class SaveAllBuffers < Red::AbstractCommand
      def define_steps
        step(:save_all_buffers)
      end

      def save_all_buffers
        BufferManager.instance.save_all_buffers
      end
    end
  end
end
