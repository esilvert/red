# frozen_string_literal: true

require 'singleton'

module Red
  ##
  # One of the few Red's manager organizing all event system
  #
  class EventManager
    include Singleton

    def initialize
      log 'Initializing'
      clear_listeners
    end

    ##
    # Allows an object to listen to the event named +event_name+ calling +callback+ on trigger
    # @param listener : The object on which the +callback+ will be called
    # @param event_name: The unique event key
    # @param callback: the callback which will receive a +call+
    #
    def listen_to(listener, event_name, callback)
      log "Listener #{listener.class.name} now listening to #{event_name} with callback #{callback}"

      @subscriptions[event_name] << { listener: listener, callback: callback }
    end

    ##
    # Triggers an event and notifies all listeners
    # @param event_name: The unique event key
    # @param args: any argument sent to the callback#call method
    #
    def trigger(event_name, *args)
      log "Triggering event #{event_name} with args #{args.join(', ')}."

      @subscriptions[event_name].count do |listener_info|
        listener = listener_info[:listener]
        if listener_info[:callback].is_a?(Symbol)
          listener.public_send(listener_info[:callback], *args)
        elsif listener_info[:callback].respond_to?(:call)
          listener_info[:callback].call(listener)
        else
          raise ArgumentError, "Unknown callback type for listner_info #{listener_info}"
        end
      end
    end

    ##
    # Clear every listeners
    # For test & initialization purposes mainly
    #
    def clear_listeners
      @subscriptions = Hash.new { |hash, key| hash[key] = [] }
    end
  end
end
