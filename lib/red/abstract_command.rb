# frozen_string_literal: true

module Red
  ##
  # Mother class of all commands that strcture the command process
  #
  class AbstractCommand
    def initialize
      @steps = []
      define_steps
    end

    ##
    # When defined a new command, override this method to define every steps
    # They will be executed in given order.
    # You might use +AbstractCommand#step+ for this
    #
    def define_steps; end

    ##
    # Store a step
    # It accepts a method name that will be +public_send+ to current object
    #
    def step(method)
      @steps << method
    end

    ##
    # Utility method to easily execute a command from anywhere
    #
    def self.call
      new.run
    end

    ##
    # Initialize the steps and start running it right away
    #
    def run
      @current_step = @steps.each.lazy
      next_step
    end

    ##
    # Switch to then execute the next step
    #
    def next_step
      public_send(@current_step.next)
    end
  end
end
