# frozen_string_literal: true

module Red
  module Ui
    class RadioButton < Tk::Tile::RadioButton
    end
  end
end
