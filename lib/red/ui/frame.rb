# frozen_string_literal: true

module Red
  module Ui
    ##
    # Proxy : Tk::Tile::Frame
    #
    class Frame < Tk::Tile::Frame
      include UiUtilities
    end
  end
end
