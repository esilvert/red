# frozen_string_literal: true

module Red
  module Ui
    class Checkbox < Tk::Tile::CheckButton
    end
  end
end
