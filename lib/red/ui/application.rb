# frozen_string_literal: true

module Red
  module Ui
    ##
    # The Red Application, handling the Tk mainloop
    #
    class Application < Tk::Root
      include LayoutUtilities
      def run
        $app = self
        Tk.mainloop
      end
    end
  end
end
