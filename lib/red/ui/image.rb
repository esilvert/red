# frozen_string_literal: true

##
# Usage:
#   Red::Image.new(:file => "myimage.gif")
#
module Red
  module Ui
    class Image < TkPhotoImage
    end
  end
end
