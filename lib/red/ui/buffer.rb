# frozen_string_literal: true

module Red
  module Ui
    ##
    # The tk view for a +Red::Buffer+
    # It uses a Red::Text as main component
    # It also has a label display the buffer name
    #
    class Buffer < Frame
      attr_reader :text
      attr_reader :label_variable

      def initialize(parent)
        super(parent)

        @text = create_text do
          # Text config
        end.grid(sticky: 'nesw')
        @text.focus

        @label_variable = Variable.new(buffer_uid)
        label_variable = @label_variable # for capture below

        @label = create_label do
          textvariable label_variable
        end.grid(sticky: 'ew')

        on_enabled
        start_listening_to_events
      end

      def buffer_uid
        "buffer_#{hash.abs}_uid"
      end

      def on_enabled
        BufferManager.instance.buffer_activated(self)
      end

      def start_listening_to_events
        @listening = true
        EventManager.instance.listen_to(self, :buffer_changed, :on_buffer_changed)
      end

      def on_buffer_changed(new_buffer_info, _previous_buffer_info)
        @text.bind_remove_all('<Modified>')

        @buffer_info = new_buffer_info
        @buffer = new_buffer_info.buffer

        @text['state'] = 'normal' # This is required as previous buffer might have locked it
        @text.delete '1.0', 'end'
        @text.insert '1.0', @buffer.content
        @text['state'] = @buffer_info.readonly? ? 'disabled' : 'normal'
        @text.see('1.0') # reset view to the top

        text = @text # For capture below
        @text.bind('<Modified>', proc do
                                   text.modified(0)
                                   @buffer.on_modified(text)
                                 end)

        @label_variable.value = @buffer.path
      end
    end
  end
end
