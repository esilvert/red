# frozen_string_literal: true

module Red
  ##
  # Include this module to easily creat all kind of Tk Entry or buffer
  #
  module LayoutUtilities
    include UiUtilities

    ##
    # Prepare Tk then create the first and only buffer on start
    #
    def initialize_layout(default_file)
      configure_tk

      create_buffer_for(default_file)
    end

    def create_buffer_for(file)
      create_buffer do
        padding '10 10 10 10'
        width 840
        height 640
        borderwidth 2
        relief 'sunken'
      end.grid(sticky: 'nesw')

      buffer = BufferManager.instance.find_or_create(file)
      BufferManager.instance.show_buffer(buffer.path)
    end

    def configure_tk
      TkGrid.columnconfigure self, 0, weight: 1
      TkGrid.rowconfigure self, 0, weight: 1
    end
  end
end
