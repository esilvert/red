# frozen_string_literal: true

module Red
  ##
  # Proxy : Tk::Tile::Spinbox
  #
  class SpinBox < Tk::Tile::Spinbox
  end
end
