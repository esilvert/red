# frozen_string_literal: true

module Red
  ##
  # Include this module to easily create every tk entity
  #
  module UiUtilities
    # rubocop:disable Style/MissingRespondToMissing
    def method_missing(name, *args, &block)
      if (match = name.to_s.match(/^create_(.+)/))
        class_underscore = match[1]
        class_name = class_underscore.split('_').map(&:capitalize).join

        Red::Ui.const_get(class_name).new(self, *args, &block)
      else
        super
      end
      # rubocop:enable Style/MissingRespondToMissing
    end
  end
end
