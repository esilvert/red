# frozen_string_literal: true

module Red
  # Proxy - Tk::Tile::Scale
  class Scale < Tk::Tile::Scale
  end
end
