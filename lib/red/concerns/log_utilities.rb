# frozen_string_literal: true

module Red
  ##
  # Include this module to easily log or create history event
  # Is included in +Object+
  #
  module LogUtilities
    def log(msg)
      Red::Help::Logger.instance.puts (respond_to?(:name) ? name : self.class.name), msg
    end

    def history(msg)
      EventManager.instance.trigger(:history, msg)
    end
  end
end
