# frozen_string_literal: true

module Red
  # Proxy - Tk::Tile::ProgressBar
  class ProgressBar < Tk::Tile::Progressbar
  end
end
