# frozen_string_literal: true

module Red
  ##
  # Proxy : TkText
  #
  class Text < TkText
  end
end
