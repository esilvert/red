# frozen_string_literal: true

module Red
  ##
  # Proxy : Tk::Tile::ScrollBar
  #
  class ScrollBar < Tk::Tile::Scrollbar
  end
end
