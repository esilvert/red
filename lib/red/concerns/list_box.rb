# frozen_string_literal: true

module Red
  ##
  # Proxy - TkListbox
  #
  class ListBox < TkListbox
  end
end
