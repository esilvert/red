# frozen_string_literal: true

module Red
  ##
  # A Buffer is the model responsible of handle a text file.
  # It is used by Ui::Buffer to be display
  # A Buffer binds an actual text file to a temporary copy.
  # At any given time, it can override one or the other of them
  #
  # You can eatiher create it blank : +Red::Buffer.new+
  # or with a path matching a actual/future text file : +Red::Buffer.new.with_path('path/to.file')
  #
  class Buffer
    attr_reader :path

    def initialize
      @current = Tempfile.new
    end

    ##
    # When called, the buffer will remember the given path and initialize its content with existing file at path
    #
    def with_path(path)
      tap do
        @path = Pathname.new(path).expand_path

        begin
          cache_current File.read(path)
        rescue Errno::ENOENT
          @new_file = true
        end
      end
    end

    ##
    # Re-read the full content of the buffer
    #
    def content
      @current.rewind
      @current.read
    end

    ##
    # Generates a +BufferInfo+ describing itself.
    # Mainly used by the +BufferMananger+
    #
    def infos
      BufferInfo.new(buffer: self, key: key, path: @path.to_s, readonly: readonly?)
    end

    ##
    # Creates or overrides target file at +@path+
    #
    def save!
      history("Saving buffer #{path}")

      FileUtils.mkdir_p @path.dirname

      File.open(@path.to_s, 'w') do |f|
        f << content
      end

      history("\tSaved.")
    end

    ##
    # Used by the +Ui::Buffer+ the know if it can be edited.
    # +SpecialBuffer+ overrides this most of the time
    #
    def readonly?
      false
    end

    ##
    # Callback - Called by +Ui::Buffer+
    # Allows to update internal copy with user's typed content
    #
    def on_modified(text)
      cache_current text.get('1.0', 'end')
    end

    ##
    # Generates a unique key for this buffer
    #
    def key
      Help::String.underscore(self.class.name, demodulize: true) + '#' + hash.abs.to_s
    end

    #
    # Private
    #
    private

    ##
    # Rewrite internal copy with +text+
    #
    def cache_current(text)
      @current.rewind
      @current << text
    end
  end
end
