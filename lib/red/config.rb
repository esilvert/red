# frozen_string_literal: true

module Red
  ##
  # Root of all configuration.
  # It loads every files in +initializers+ directory
  #
  module Config
    def self.load
      (Pathname.new(__dir__) + 'initializers').children.each do |file|
        require file.to_s
      end
    end
  end
end
