# frozen_string_literal: true

require 'singleton'

module Red
  ##
  # One of the few Red's managers.
  # The BindingManager allows a mode to be activated
  # To do so, it must register with a given +matcher+ which will decide for each buffer focus
  #   if it should be enabled
  #
  class BindingManager
    include Singleton

    attr_reader :application

    def initialize
      log 'Initializing'

      @modes = [{ mode: Binding::ApplicationMode.new, match: true }]
    end

    ##
    # Registers a mode for auto activation.
    # +mode+ the mode itself
    # +matcher+ returns wether a mode should be enabled or not
    #
    def register_mode(mode:, matcher:)
      log "Registering mode #{mode.class.name}"

      @modes << { mode: mode, matcher: matcher }
    end

    ##
    # Binds the manager to a Red application
    #
    def bind_application(application)
      @application = application
    end

    ##
    # Callback - Called when a Ui::Buffer is activated.
    # Triggers all modes that matches it
    #
    def buffer_activated(buffer)
      log "Evaluating buffer #{buffer.label_variable.value}"

      @modes.each do |hash|
        hash[:mode].on_enabled(buffer.text) if hash[:match]
      end
    end
  end
end
