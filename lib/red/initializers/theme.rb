# frozen_string_literal: true

# Theme.rb

Tk::Tile::Style.configure(
  'Danger.TFrame',
  'background' => 'red',
  'borderwidth' => 5,
  'relief' => 'raised'
)
