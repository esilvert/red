# frozen_string_literal: true

module Red
  module Help
    ##
    # Development helper
    #
    module Debug
      ##
      # Recursively print a hierarchy starting from +node+
      #
      def self.print_hierarchy(node, depth = 0)
        print('  ' * depth +
              node.winfo_class +
              ' w=' + node.winfo_width.to_s +
              ' h=' + node.winfo_height.to_s +
              ' x=' + node.winfo_x.to_s +
              ' y=' + node.winfo_y.to_s +
              "\n")

        node.winfo_children.each do |i|
          print_hierarchy(i, depth + 1)
        end
      end
    end
  end
end
