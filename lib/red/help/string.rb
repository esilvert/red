# frozen_string_literal: true

module Red
  module Help
    ##
    # String manipulation helper
    #
    module String
      ##
      # Removes modules from a string
      # ex: Red::Ui::Buffer -> Buffer
      #
      def self.demodulize(string)
        string.to_s.split('::').last
      end

      ##
      # Transforms a string to its underscore / snake case version
      # From : https://apidock.com/rails/ActiveSupport/Inflector/underscore
      #
      def self.underscore(camel_cased_word, demodulize: false)
        return camel_cased_word unless /[A-Z-]|::/.match?(camel_cased_word)

        word = if demodulize
                 self.demodulize(camel_cased_word)
               else
                 camel_cased_word.to_s.gsub('::', '/')
               end

        acronyms_underscore_regex = /(?:(?<=([A-Za-z\d]))|\b)((?=a)b)(?=\b|[^a-z])/.freeze
        word.gsub!(acronyms_underscore_regex) do
          "#{Regexp.last_match(1) && '_'}#{Regexp.last_match(2).downcase}"
        end

        word.gsub!(/([A-Z\d]+)([A-Z][a-z])/, '\1_\2')
        word.gsub!(/([a-z\d])([A-Z])/, '\1_\2')
        word.tr!('-', '_')
        word.downcase!
        word
      end

      ##
      # Transforms a string to its camelized version
      #
      def self.camelize(underscore_word)
        word = underscore_word.dup
        word.gsub!(' ', '_')
        word.split('_').map(&:capitalize).join
      end

      ##
      # Get a constant from its full description
      # Use +Constant#get_from_name+ for better granularity
      #
      def self.constantize(name)
        Object.const_get(name)
      end
    end
  end
end
