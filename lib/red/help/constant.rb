# frozen_string_literal: true

module Red
  module Help
    ##
    # Constant manipulation helper
    #
    module Constant
      ##
      # Camelize the given +name+
      #
      def self.get_name(name)
        name.to_s.split('_').map(&:capitalize).join
      end

      ##
      # Retrieve a constant from its name and +base+ namespace
      #
      def self.get_from_name(name, base: Object)
        constant = get_name(name)
        base.const_get(constant)
      end

      ##
      # Returns a unique id for an object
      #
      def self.unique_id(object)
        "#{object.class.name}_#{object.hash.abs}_uid"
      end

    end
  end
end
