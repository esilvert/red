# frozen_string_literal: true

require 'singleton'

module Red
  module Help
    ##
    # Manage logging
    #
    class Logger
      include Singleton

      def initialize
        self.puts 'Log', "Using logfile at #{log_file.path}"
      end

      ##
      # Allows to write in the log file
      #
      def <<(tags, msg)
        tags = case tags.class.name
               when Array
                 tag.map { |t| "[#{t}]" }.join
               else
                 "[#{tags}]"
               end

        line = format('%-30s     %-80s', tags, msg)

        unless Red.env.test? && ENV['VERBOSE'].nil?
          console_line = format("\e[96m%-30s     \e[93m%-80s\e[0m", tags, msg)
          STDOUT.puts console_line
        end

        log_file << line
      end
      alias puts <<

      #
      # Private
      #
      private

      ##
      # Returns the file where the logging occurs
      #
      def log_file
        @log_file ||= Tempfile.new
      end
    end
  end
end
$logger = Red::Help::Logger.instance
