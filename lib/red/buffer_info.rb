# frozen_string_literal: true

module Red
  ##
  # A +BufferInfo+ describes a +Red::Buffer+
  # +buffer+ : the actual buffer it describes
  # +key+ : A unique key to define the buffer
  # +path+ : If present, the path where the buffer points
  # +readonly+ : Is the update allowed or not
  #
  class BufferInfo
    attr_reader :buffer, :key, :path, :readonly

    def initialize(buffer:, key:, path:, readonly:)
      @buffer = buffer
      @key = key
      @readonly = readonly
      @path = path
    end

    ##
    # Proxy - Allows the buffer to respond for the buffer_info
    #
    def method_missing(name, *args, &block)
      if @buffer.respond_to?(name)
        @buffer.public_send(name, *args, &block)
      else
        super
      end
    end

    def respond_to_missing?(method_name, include_private = false)
      @buffer.respond_to?(method_name) || super
    end

    ##
    # Fetch the basename from the +@path+
    #
    def basename
      Pathname.new(path).basename.to_s
    end

    ##
    # Trying to fix an issue with the display of +BufferInfo+ in console
    #
    def to_s
      "<BufferInfo #{path} with readonly #{readonly}>"
    end
  end
end
