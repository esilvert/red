# frozen_string_literal: true

module Red
  ##
  # The Red::BufferManager bufferstack
  # It handles its navigation history, opening and closing them
  #
  class BufferStack
    def initialize
      @stack = []
    end

    ##
    # Return the current buffer
    #
    def current_buffer
      @stack.first
    end

    ##
    # Return the first that matches name (might conflict)
    #
    def find_by_name(name)
      result = @stack.detect do |buffer_info|
        Pathname.new(buffer_info.path).expand_path.to_s.match?(Regexp.new("#{name}$"))
      end

      update_current(result)

      result
    end

    ##
    # Find a special buffer by its name
    # ex: "*scratch*"
    #
    def find_by_special_name(name)
      result = @stack.detect do |buffer_info|
        buffer_info.key == name
      end

      update_current(result)

      result
    end

    ##
    # Update stack to put the +buffer_info+ at the top of it
    #
    def update_current(buffer_info)
      @stack.delete(buffer_info)
      @stack.insert(0, buffer_info)
    end

    def method_missing(name, *args, &block)
      if @stack.respond_to?(name)
        @stack.public_send(name, *args, &block)
      else
        super
      end
    end

    def respond_to_missing?(method_name, include_private = false)
      @stack.respond_to?(method_name) || super
    end
  end
end
