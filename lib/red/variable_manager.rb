# frozen_string_literal: true

require 'singleton'

module Red
  ##
  # One of the few Red's managers registering every Red::Variable
  #
  class VariableManager
    include Singleton

    def initialize
      log 'Initializing'
      @variables = {}
    end

    def register(key, owner)
      raise ArgumentError, "Variable already registered #{key}" if @variables.key?(key)

      log "Defined variable #{key}"
      @variables[key] = owner
    end

    def access(key)
      raise ArgumentError, "Unknown variable #{key}" unless @variables.key?(key)

      @variables[key]
    end
  end
end
