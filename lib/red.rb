# frozen_string_literal: true

require 'red/version'

require 'tk'
require 'tkextlib/tile'

require 'tempfile'
require 'ostruct'

# dev
require 'byebug'

# Setup Zeitwerk
require 'zeitwerk'

loader = Zeitwerk::Loader.new
loader.tag = File.basename(__FILE__, '.rb')
loader.inflector = Zeitwerk::GemInflector.new(__FILE__)
loader.push_dir(__dir__)

# Special namespaces
%w[concerns].each do |dir|
  loader.collapse("#{__dir__}/*/#{dir}/")
end

# Ignore folders
%w[initializers].each do |dir|
  loader.ignore("#{__dir__}/*/#{dir}/")
end

loader.enable_reloading
loader.setup # Actually loads, must be before red module
loader.eager_load

# Extensions
Object.include(Red::LogUtilities)

# Config
Red::Config.load

##
# Red module is used to start the application. It instantiates then runs the application/
#
module Red
  class Error < StandardError; end

  ##
  # A Pathname pointing to red root folder
  #
  ROOT_PATH = (Pathname.new(__dir__) + 'red').freeze

  ##
  # A Pathname pointing to red resources folder
  #
  RESOURCE_PATH = ROOT_PATH + 'resources'

  ##
  # The Red total execution, with initialization and mainloop
  #
  def self.run(**options)
    log "Intializing Application with options #{options}"

    @application = Ui::Application.new(**options.slice(:title)).tap do |application|
      log "Initializing #{options[:files].size} buffers"

      options[:files].each do |file|
        BufferManager.instance.create_buffer(file)
      end

      log 'Intializing Layout'
      application.initialize_layout(options[:files].first)

      log 'Intializing Bindings'
      BindingManager.instance.bind_application(application)
    end

    @application.run
  end
end
